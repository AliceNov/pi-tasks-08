#include "Matrix.h"
#include <fstream>
#define num 3
using namespace std;

int main()
{
	srand(time(0));
	ifstream input("filein.txt");
	ofstream output("fileout.txt");
	Matrix M1(3, 3), M2(3, 3);
	input >> M1 >> M2;
	output << M1 << endl << M2;
	output << endl << "The sum of the matrices" << endl << M1 + M2 << endl;
	output << endl << "The subtraction of matrices" << endl << M1 - M2 << endl;
	output << endl << "The multiplication of matrices" << endl << M1*M2 << endl;
	output << "Thu multiplication first matrix by number " << num << "=" << endl << M1*num << endl;
	output << "The devide first mutrix by number " << num << "=" << endl << M1 / num << endl;
	if (M1 == M2)
		output << "First matrix is equal to second matrix" << endl;
	else if (M1 != M2)
		output << "First matrix isn`t equal to second matrix" << endl;
	return 0;
}